
#define __SCROLL_IMPL__
#include "MyGame.h"
#undef __SCROLL_IMPL__

#include "Ship.h"
#include "Alien.h"

/* 
 * This is a basic C++ template to quickly and easily get started with a project or tutorial.
 */

orxSTATUS orxFASTCALL PhysicsEventHandler(const orxEVENT *_pstEvent)
{
	if (_pstEvent->eType == orxEVENT_TYPE_PHYSICS) {

		if (_pstEvent->eID == orxPHYSICS_EVENT_CONTACT_ADD) {
			orxOBJECT *pstRecipientObject, *pstSenderObject;

			/* Gets colliding objects */
			pstRecipientObject = orxOBJECT(_pstEvent->hRecipient);
			pstSenderObject = orxOBJECT(_pstEvent->hSender);

			const orxSTRING recipientName = orxObject_GetName(pstRecipientObject);
			const orxSTRING senderName = orxObject_GetName(pstSenderObject);

			if (orxString_Compare(senderName, "Alien") == 0) {
				if (orxString_SearchString(recipientName, "Ship") != orxNULL) {
					int here = 1;
				}
			}

			if (orxString_Compare(recipientName, "Alien") == 0) {
				if (orxString_SearchString(senderName, "Ship") != orxNULL) {
					int here = 1;
				}
			}
		}
	}

	return orxSTATUS_SUCCESS;
}


orxSTATUS MyGame::Init()
{
	orxSTATUS result = orxSTATUS_SUCCESS;
	//orxViewport_CreateFromConfig("Viewport");
	//orxObject_CreateFromConfig("Ship");

	score = 0;
	level = 5;

	//CreateObject("Ship");
	orxObject_CreateFromConfig("Playfield");
	levelObject = orxObject_CreateFromConfig("LevelSetup");
	
	scoreObject = orxObject_CreateFromConfig("Score");

	//orxEvent_AddHandler(orxEVENT_TYPE_PHYSICS, PhysicsEventHandler);
	return result;
}

void MyGame::BindObjects()
{
	ScrollBindObject<Ship>("Ship");
	ScrollBindObject<Alien>("Alien");
}

/** Run function, is called every clock cycle
 */
orxSTATUS MyGame::Run()
{
	orxSTATUS eResult = orxSTATUS_SUCCESS;

	/* Should quit? */
	if(orxInput_IsActive("Quit"))
	{
		/* Updates result */
		eResult = orxSTATUS_FAILURE;
	}

	if (orxInput_IsActive("Create") && orxInput_HasNewStatus("Create"))
	{
		levelObject = orxObject_CreateFromConfig("LevelSetup");
	}

	if (orxInput_IsActive("Delete") && orxInput_HasNewStatus("Delete"))
	{


	}

	/* Done! */
	return eResult;
}

/** Exit function
 */
void MyGame::Exit()
{
}

void MyGame::AddToScore(int value)
{
	score += value;

	orxCHAR acBuffer[256];

	// Updates score
	orxString_NPrint(acBuffer, 256, "%d", score);

	// Updates it
	orxObject_SetTextString(scoreObject, acBuffer);
}

int MyGame::GetScore()
{
	return score;
}

void MyGame::IncreaseNextLevelPoints() {
	pointsToNextLevel++;

	if (pointsToNextLevel >= POINTS_TO_LEVEL) {
		pointsToNextLevel = 0;

		orxObject_SetLifeTime(levelObject, 0);

		orxConfig_PushSection("CurrentLevel");

		level = orxConfig_GetS32("Number");
		level++;

		orxFLOAT delay = orxConfig_GetFloat("Delay");
		delay = delay - 0.5;
		if (delay < 1) {
			delay = 1;
		}
		orxConfig_SetFloat("Delay", delay);

		orxConfig_SetS32("Number", level);
		orxConfig_PopSection();

		levelObject = orxObject_CreateFromConfig("LevelSetup");
	}
}

void MyGame::ResetNextLevelPoints() {
	pointsToNextLevel = 0;
}

/** Locate the starting config .ini file
 */
orxSTATUS MyGame::Bootstrap() const
{
	// Add "../data/config" to the list of locations that config files can be loaded from
	orxResource_AddStorage(orxCONFIG_KZ_RESOURCE_GROUP, "../../data/config", orxFALSE);

	// Loads a specific config file 
	orxConfig_Load("MyGame.ini");

	return orxSTATUS_FAILURE;
}

/** Main function
 */
int main(int argc, char **argv)
{

	// Executes game
	MyGame::GetInstance().Execute(argc, argv);

	// Done!
	return EXIT_SUCCESS;
}

#ifdef __orxMSVC__

//// Here's an example for a console-less program under windows with visual studio
//int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
//{
//  // Inits and executes orx
//  orx_WinExecute(Init, Run, Exit);
//
//  // Done!
//  return EXIT_SUCCESS;
//}

#endif // __orxMSVC__
