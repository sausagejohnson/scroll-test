#include "Alien.h"

void Alien::OnCreate()
{
	//// Set initial movement direction
	//m_direction = SOUTH;
	//// Get movement speed from config value 
	//m_movementSpeed = orxConfig_GetFloat("MovementSpeed");
	//// Get direction change interval from config value
	//m_directionChangeInterval = orxConfig_GetFloat("DirectionChangeInterval");
	//movementSpeed = 400;
}

void Alien::OnDelete()
{
	// Do nothing when deleted
}

void Alien::Update(const orxCLOCK_INFO &_rstInfo)
{


	//// Time since direction change exceeds interval of direction change?
	//if ((m_timeSinceDirectionChange += _rstInfo.fDT) >= m_directionChangeInterval)
	//{
	//	// Reset time
	//	m_timeSinceDirectionChange = 0;
	//	// Pick random number between bounds of Direction enum
	//	orxU32 randomNum = orxMath_GetRandomU32(0, highDirection);
	//	// Update object's direction of movement
	//	m_direction = static_cast<Direction> (randomNum);
	//}
}

orxBOOL Alien::OnCollide(ScrollObject *_poCollider, 
	const orxSTRING _zPartName, 
	const orxSTRING _zColliderPartName, 
	const orxVECTOR &_rvPosition, 
	const orxVECTOR &_rvNormal)
{
	// Add flash effect
	//AddFX("FX-Flash");
	if (_poCollider == orxNULL) {
		return orxTRUE;
	}

	const orxSTRING colliderName = _poCollider->GetModelName();
	if (orxString_Compare(colliderName, "Laser") == 0) {
		_poCollider->SetLifeTime(0);

		orxVECTOR position = orxVECTOR_0;
		GetPosition(position, orxTRUE);

		orxOBJECT *explosion = orxObject_CreateFromConfig("Explosion");
		orxObject_SetPosition(explosion, &position);

		MyGame::GetInstance().IncreaseNextLevelPoints();
	}

	SetLifeTime(0);

	MyGame::GetInstance().AddToScore(150);

	//const orxSTRING colliderName = _poCollider->GetModelName();
	//if (orxString_Compare(colliderName, "Laser") == 0) {
	//	_poCollider->SetLifeTime(0);
	//}

	return orxTRUE;
}