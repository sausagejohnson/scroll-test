#ifndef __MYGAME_H_
#define __MYGAME_H_

//! Includes
// The following define skips compilation of ScrollEd (map editor) for now
#define __NO_SCROLLED__
#include "Scroll.h"

//! MyGame class
class MyGame : public Scroll<MyGame>
{
public:
	virtual void	AddToScore(int value);
	virtual int		GetScore();
	virtual void	IncreaseNextLevelPoints();
	virtual void	ResetNextLevelPoints();

private:
	int score;
	int level;
	int pointsToNextLevel;
	const int POINTS_TO_LEVEL = 10;

	//! Initialize the program
	virtual orxSTATUS Init();
	//! Callback called every frame
	virtual void BindObjects();
	virtual orxSTATUS Run();
	//! Exit the program
	virtual void      Exit();

	orxOBJECT *levelObject;
	orxOBJECT *scoreObject;

	virtual orxSTATUS Bootstrap() const;

};

#endif // __MYGAME_H_